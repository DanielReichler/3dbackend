text1 = "##TEXT1##";
text2 = "##TEXT2##";
text3 = "##TEXT3##";

mode = "cut";

// static vars for iPhoneX Case.stl
casewallheight = 0;
casewallthickness = 1.6;

if ( mode == "cut") {
    difference() {
        importBaseCase();
        addText(text1, text2, text3, font, size);
    }
}

module importBaseCase() {
    translate([0,0,0])
       rotate([0,180,0])
        import_stl("kavianCase.stl", convexity=1, center=true);
}

module addText(text1, text2, text3, font, size) {
    deepthfactor = 0.5;
    securityMargin = 1;

    translate([0,0,-(casewallheight + casewallthickness * deepthfactor) ])
        linear_extrude(height = (casewallthickness * deepthfactor) + securityMargin, convexity = 5)
                multiLine() {
                    text(text1, font = font, size = size, halign = "center");
                    text(text2, font = font, size = size, halign = "center");
                    text(text3, font = font, size = size, halign = "center");
                }
}

module multiLine(){
  union(){
    for (i = [0 : $children-1])
      translate([0 , -i * 12, 0 ]) children(i);
  }
}