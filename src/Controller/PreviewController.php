<?php

namespace App\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Render3d\Customizer\ApiCustomizer;

class PreviewController extends Controller
{
    /**
     * @Route("/healthcheck", name="healthcheck")
     */
    public function healtcheck()
    {
        return $this->render('preview/healthcheck.html.twig', [
        ]);
    }

    /**
     * @Route("/preview", name="preview", methods={"GET"})
     */
    public function index(Request $request, LoggerInterface $logger)
    {
        $filesystem = new Filesystem();

        if ($filesystem->exists('/tmp') !== true) {
            $filesystem->mkdir('/tmp');
        }

        $randomId = uniqid(15);

        $filesystem->mkdir('/tmp/g' . $randomId);

        $scadfile = $this->get('kernel')->getRootDir() . '/../files/iPhoneXModifierAPI_speed.scad';

        $render3d = new ApiCustomizer();

        $workingDir = "/tmp/g" . $randomId;
        $logger->info('Workingdir is ' . $workingDir);

        $neededFiles = [
            'iPhoneXModifierAPI_speed.scad',
            'kavianCase.stl',
        ];

        foreach ($neededFiles as $neededFile) {
            $fileUrl = 'https://3dfiles.ams3.digitaloceanspaces.com/renderer/' . $neededFile;
            $fileDest = $this->get('kernel')->getRootDir() . '/../files/' . $neededFile;


            /*if ($this->container->get('kernel')->getEnvironment() === "dev")
                $filesystem->copy($fileDest, $workingDir . '/' . $neededFile);
            else
                file_put_contents($fileDest, fopen($fileUrl, 'r'));*/

            $filesystem->copy($fileDest, $workingDir . '/' . $neededFile);
        }

        $render3d->workingDir($workingDir);

        $render3d->executable('libre3d', 'openscad');
        //$render3d->executable('libre3d', $this->get('kernel')->getRootDir() . '/../executable/openScad.AppImage');

        $inputParams = json_decode($request->get('printParams'));

        $params = [];

        foreach ($inputParams as $inputParam => $value) {
            $params[strtoupper($inputParam)] = $value;
        }

        /*$params = [
            'TEXT1' => !is_null($request->get('text1')) ? $request->get('text1') : '',
            'TEXT2' => !is_null($request->get('text2')) ? $request->get('text2') : '',
            'TEXT3' => !is_null($request->get('text3')) ? $request->get('text3') : '',
            'MODE' => !is_null($request->get('mode')) ? $request->get('mode') : 'impress',
            'FONTFAMILY' => !is_null($request->get('fontfamily')) ? $request->get('fontfamily') : 'Libertas Sans',
            'FONTSIZE' => intval(!is_null($request->get('fontsize')) ? $request->get('fontsize') : '8'),
            'POSX' => intval(!is_null($request->get('posx')) ? $request->get('posx') : '0'),
            'POSY' => intval(!is_null($request->get('posy')) ? $request->get('posy') : '0'),
            'ROTATEDEGREE' => intval(!is_null($request->get('rotatedegree')) ? $request->get('rotatedegree') : '0'),
        ];*/

        //@ echo "Successful executable povray...<br>";
        try {
            // This will copy in your starting file into the working DIR if you give the full path to the starting file.
            // This will also set the fileType for you.
            //$render3d->filename("/app/files/" . $scadfile);
            $render3d->Apifilename($params,$scadfile);

            $renderedImagePath = $render3d->render('stl');

        } catch (Exception $e) {
            if ($this->container->get('kernel')->getEnvironment() !== "dev")
                $filesystem->remove('/tmp/g' . $randomId);

            return $this->json(['error' => true]);
        }

        $filesystem->remove('/tmp/g' . $randomId);

        return $this->render('empty.html.twig', [
            'data' => $renderedImagePath
        ]);
    }
}
