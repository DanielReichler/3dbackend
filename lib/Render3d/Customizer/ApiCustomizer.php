<?php

namespace Render3d\Customizer;

/**
 * Customizer class extends from Render3d class.
 * run php composer.phar dump-autoloader when added new class
 */
class ApiCustomizer extends Render3d {
	/*@
	 * Read scad file and extract parameters.
	 * 
	 * @return parameters in 2D array
	 */
	 public function readSCAD($scadfile = null) {
	 	if(!empty($scadfile)) {
	 		//@ get file info, fielname, dirname
	 		$pathInfo = pathinfo($scadfile);
	 		//@ check if it is scad file
	 		if(strtolower($pathInfo['extension']) !== "scad") {
	 			return '';
	 		}
	 		//@ open scad file to read
		 	$openscad = fopen($scadfile,"r") or die("Unable to open scad file!");
		 	//@ create new scad filename
		 	//$filename = (strlen($pathInfo['filename']) > 50) ? substr($pathInfo['filename'],0,50) : $pathInfo['filename'];
		 	//$newfilename = $pathInfo['dirname']."/".$filename."_".date('Y_m_d_H-i-s').".scad";
		 	//@ open new scad file to write
			//$newscad = fopen($newfilename,"w") or die("Unable to create new scad file!");
			//@ 2D array variable
			$result = array();
			$indexArray = 0;
			//@read from original scad file to the new one
		 	while(!feof($openscad)) {
	  			$string = fgets($openscad);
	  			if(strpos($string,"{") !== false) {
	  					//@ first { , no more parameter
	  					break;
	  			}
	  			if(strpos($string,"(") == false) {
	  				//@ no parenthesis in the line
	  				
	  				if($string[0] !== "\n" && strpos($string, "use") === false) {
		  				//@ not empty line
			  			$strCheck = substr($string,0,2);
			  			if($strCheck == "//") {
			  				//@ there is description
			  				$descriptionStr = $string;
			  			}
			  			else {
			  				//@ variable line
			  				if(strpos($string,"+") == false && strpos($string,"*") == false && strpos($string,"-") == false) {
			  					//@ no calculation with/without other variables, otherwise ignore
				  				list($part1,$part2) = explode("=",$string);
				  				$varName = str_replace(' ','',$part1);
				  				list($part1,$part2) = explode(";",$part2);
				  				//@ remove space and double quote
				  				$varValue = str_replace(' ','',$part1);
				  				$varValue = str_replace('"','',$varValue);
				  				//@ get rid of white space at the end from explode func.
				  				$part2 = trim($part2);
				  				if(strpos($varValue,"/") == false) {
					  				if(!empty($part2)) {
					  					//@ there is possible values
					  					$possibleVal = $part2;
					  				}
					  				else {
					  					//@ no possible value
					  					$possibleVal = '';
					  				}
					  				//@ put into array
					  				$result[$indexArray] = array();
					  				$result[$indexArray][0] = $descriptionStr;
					  				$result[$indexArray][1] = $varName;
					  				$result[$indexArray][2] = $varValue;
					  				$result[$indexArray][3] = $possibleVal;
					  				$indexArray += 1;
					  			}
			  				}
			  			}
			  		}
			  		else {
			  			//@ empty line
			  			$descriptionStr = '';
			  		}
		  		}
		  		
			}

			fclose($openscad);
			//fclose($newscad);
		}
		else {
			//@ return empty if no filename
			return '';
		}
		return $result;
	 }
	 /*@
	 * Write new customized scad file with a new name.
	 * 
	 * @return void
	 */
	 public function writeSCAD($scadfile = null,$result = null) {
	 	//echo "in writeSCAD function...<br>";
	 	//@ get file info, fielname, dirname
	 		$pathInfo = pathinfo($scadfile);
	 		//@ open scad file to read
	 		//echo "original scad file ".$scadfile." ...<br>";
		 	$openscad = fopen($scadfile,"r") or die("Unable to open scad file!");
		 	//@ create new scad filename
		 	$filename = (strlen($pathInfo['filename']) > 50) ? substr($pathInfo['filename'],0,50) : $pathInfo['filename'];
		 	$newscadname = $filename."_".date('Y_m_d_H-i-s').".scad";
		 	$newfilename = $pathInfo['dirname']."/".$newscadname;
		 	//@ open new scad file to write
			$newscad = fopen($newfilename,"w") or die("Unable to create new scad file!");
			//@ write the updated parameters to a new scad file
			
			//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			$indexArray = 0;			
			while(!feof($openscad)) {
	  			$string = fgets($openscad);
	  			if(strpos($string,"{") !== false) {
	  				//@ first { , no more parameter
	  				//@ start writing
	  				fwrite($newscad, $string);
	  				break;
	  			}
	  			if(strpos($string,"(") == false) {
	  				//@ no parenthesis in the line
	  				if($string[0] !== "\n" ) {
		  				//@ not empty line
			  			$strCheck = substr($string,0,2);
			  			if($strCheck == "//") {
			  				//@ there is description or just comment
			  				fwrite($newscad, $string);
			  			}
                        if(strpos(substr($string,0,3), "use") !== false) {
                            //@ there is a use statement
                            fwrite($newscad, $string);
                        }
			  			else {
			  				//@ variable line
			  				if(strpos($string,"+") == false && strpos($string,"*") == false && strpos($string,"-") == false) {
			  					//@ no calculation with/without other variables, otherwise ignore
				  				list($part1,$part2) = explode("=",$string);
				  				//$varName = str_replace(' ','',$part1);
				  				list($part1,$part2) = explode(";",$part2);
				  				$varValue = str_replace(' ','',$part1);
				  				if(strpos($varValue,"/") == false) {
					  				//@ variable name, value, and possible value
									//@ if possible value is not empty
									if(!empty($result[$indexArray][3])) {
										if(ctype_alpha($result[$indexArray][2])) {
											fwrite($newscad, $result[$indexArray][1]." = ".'"'.$result[$indexArray][2].'"'."; ".$result[$indexArray][3]."\n");
										}
										else {
											fwrite($newscad, $result[$indexArray][1]." = ".$result[$indexArray][2]."; ".$result[$indexArray][3]."\n");
										}
									}
									else {
										//@ no possible value comment
										if(ctype_alpha($result[$indexArray][2])) {
											fwrite($newscad, $result[$indexArray][1]." = ".'"'.$result[$indexArray][2].'"'."; \n");
										}
										else {
											fwrite($newscad, $result[$indexArray][1]." = ".$result[$indexArray][2]."; \n");
										}
									}
					  				$indexArray += 1;
					  			}
					  			else {
					  				//@ ignore variable with calculation
			  						fwrite($newscad, $string);
					  			}
			  				}
			  				else {
			  					//@ ignore variable with calculation
			  					fwrite($newscad, $string);
			  				}
			  			}
			  		}
			  		else {
			  			//@ empty line
			  			fwrite($newscad, "\n");
			  		}
		  		}
		  		else {
		  			//@ function contain ()
		  			fwrite($newscad, $string);
		  		}
		  		
			}//@ end while(!feof($openscad))
			//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			
			//@ write the rest from the original file
			while(!feof($openscad)) {
				//@ write the rest of the file, no more check
	  			$string = fgets($openscad);
	  			fwrite($newscad, $string);
			}
			//@ read until found {, then start writing the rest

			fclose($openscad);
			fclose($newscad);
			
			//@ return new scad filename
			return $newscadname;
	 }
	 /*@
	 * Generate variable name in bold
	 * 
	 * @return html format variable name
	 */
	 private function generateVarname($varName = null) {
	 	if(!empty($varName)) {
	 		return "<b>". ucwords(str_replace('_',' ',$varName)) ."</b>";
	 	}
	 }
	  /*@
	 * Generate html element for customization
	 * 
	 * @return html element string
	 */
	 public function generateElement($dataRow = NULL,$index) {
	 	//@ 0:description, 1:variable name, 2: value, 3:possible value
	 	//@ var_dump($dataRow);
	 	if(!empty($dataRow[3]) && strpos($dataRow[3],"[") !== false && strpos($dataRow[3],"]") !== false) {
	 		//@ remove comment sign
 			$possibleVal = str_replace('/','',$dataRow[3]);
 			//@ remove space
 			$possibleVal = str_replace(' ','',$possibleVal);
 			//@ remove []
 			$possibleVal = str_replace('[','',$possibleVal);
 			$possibleVal = str_replace(']','',$possibleVal);
	 		//@ generate dropdown and slider
	 		if(strpos($possibleVal,",") !== false) {
	 			//@ with comma in possible values, drop down box
	 			$dataRow[2] = trim($dataRow[2]);
	 			//@ $dataRow[2] = str_replace('"','',$dataRow[2]);
	 			//@ create select element
	 			$result = "<p>". self::generateVarname($dataRow[1]) ." ".$dataRow[0]."</p><p><select name='var".$index."' >";
	 			//@ check label or not?
	 			if(strpos($possibleVal,":") !== false) {
	 				//@ option with label
	 				$options = explode(",",$possibleVal);
	 				foreach ($options as $option) {
	 					$option = trim($option);
	 					list($val,$label) = explode(":",$option);
	 					$val = trim($val);
	 					$label = trim($label);
	 					if($val == $dataRow[2]) {
	 						$result .= " <option value='".$val."' selected>".$label."</option>";
	 					}
	 					else {
	 						$result .= " <option value='".$val."'>".$label."</option>";
	 					}
	 				}
	 			}
	 			else {
	 				//@ option with no label
	 				$options = explode(",",$possibleVal);
	 				foreach ($options as $option) {
	 					$option = trim($option);
	 					if($option == $dataRow[2]) {
	 						$result .= " <option value='".$option."' selected>".$option."</option>";
	 					}
	 					else {
	 						$result .= " <option value='".$option."'>".$option."</option>";
	 					}
	 				}
	 			}
	 			$result .= "</select></p>";
	 			return $result;
	 		}
	 		else {
	 			//@ no comma in possible values, slider
	 			if(strpos($possibleVal,":") == false) {
	 				//@ only max value, min is 0
	 				//@ create range element
	 				$result = "<p>". self::generateVarname($dataRow[1]) ." ".$dataRow[0]."</p><p><input type=\"range\" name=\"var".$index."\" min=\"0\" max=\"".trim($possibleVal)."\" value=\"".trim($dataRow[2])."\" oninput=\"showValue(this.value,'range".$index."')\"><span id=\"range".$index."\">".trim($dataRow[2])."</span></p>";
	 			}
	 			else {
	 				//@ min/max, min/step/max
	 				$values = explode(":",$possibleVal);
	 				$num = count($values);
	 				if($num == 2) {
	 					//@ only min and max
	 					$result = "<p>". self::generateVarname($dataRow[1]) ." ".$dataRow[0]."</p><p><input type=\"range\" name=\"var".$index."\" min=\"".trim($values[0])."\" max=\"".trim($values[1])."\" oninput=\"showValue(this.value,'range".$index."')\" value=\"".trim($dataRow[2])."\" ><span id=\"range".$index."\">".trim($dataRow[2])."</span></p>";
	 				}
	 				else {
	 					//@ min,max,and step
	 					$result = "<p>". self::generateVarname($dataRow[1]) ." ".$dataRow[0]."</p><p><input type=\"range\" name=\"var".$index."\" min=\"".trim($values[0])."\" max=\"".trim($values[2])."\" step=\"".trim($values[1])."\" value=\"".trim($dataRow[2])."\" oninput=\"showValue(this.value,'range".$index."')\"><span id=\"range".$index."\">".trim($dataRow[2])."</span></p>";
	 				}
	 			}
	 			return $result;
	 		}
	 	}
	 	else {
	 		//@ generate textbox, no possible value
	 		return "<p>". self::generateVarname($dataRow[1]) ." ".$dataRow[0]."</p><p><input type='text' name='var".$index."' value='".$dataRow[2]."'></p>";
	 	}
	 }

    /**
     * Render the file and return the full path to the rendered image.
     *
     * @param string $engine
     * @param array $options
     * @return string Full path to rendered image file
     * @throws \Exception
     */
    public function render ($engine = 'povray', $options = null) {
        //@echo 'in Render3d.render...<br>';
        if (empty($this->fileType) || empty($this->workingDir)) {
            // File type or working dir not set
            throw new \Exception('Filetype and Workingdir must be set first.');
        }
        // Set the options
        if (!empty($options)) {
            $this->options($options);
        }
        //@echo 'engine '.$engine.'<br>';
        $this->startBuffer();


        $currentDir = getcwd();

        //we need to be in base directory for all the rendering stuff to work...
        chdir($this->workingDir);

        $renderer = $this->getRenderer($engine);
        try {
            $result = $renderer->render();
        } catch (\Exception $e) {
            chdir($currentDir);
            $this->stopBuffer();
            throw $e;
        }

        // Now go back to the starting dir
        chdir($currentDir);

        $this->stopBuffer();
        return $result;
    }
    /**
     * Convert from the current file type to the one given.
     *
     * @param string $fileType The "end" file type to convert to.
     * @param array $options Array of options for the specific conversion
     * @return void
     * @throws \Exception
     */
    public function convertTo ($fileType, $options = null) {
        if (empty($this->fileType) || empty($this->workingDir)) {
            // File type or working dir not set
            throw new \Exception('Filetype and working dir are not set.');
        }

        // Set the options
        if (!empty($options)) {
            $this->options($options);
        }
        $this->startBuffer();

        $currentDir = getcwd();

        //we need to be in base directory for all the rendering stuff to work...
        chdir($this->workingDir);

        //$converter = $this->getConverter($this->fileType, $fileType);
        $converter = $this->getConverter($this->fileType, $fileType);
        try {
            $result = $converter->apiConvert();
        } catch (\Exception $e) {
            chdir($currentDir);
            $this->stopBuffer();
            throw $e;
        }

        // Now go back to the starting dir
        chdir($currentDir);
        $this->stopBuffer();

        return $result;
    }

    public function Apifilename($params, $filename = null) {
        if (!empty($filename)) {
            $pathInfo = pathinfo($filename);
            //@echo 'filename is not empty...<br>';
            //@echo 'dirname: '.$pathInfo['dirname'].'<br>';
            //@if(file_exists($filename)) {echo $filename.' is exist...<br>';}
            if (!empty($pathInfo['dirname']) && $pathInfo['dirname'] !== '.' && file_exists($filename)) {
                //@echo 'dirname: '.$pathInfo['dirname'].'<br>';
                if (empty($this->workingDir)) {
                    // Set the working dir based on the file path
                    throw new \Exception('Working directory required.');
                }
                //@ echo 'working directory: '.$this->workingDir.'<br>';
                //@echo 'basename: '.$pathInfo['basename'].'<br>';
                // Copy it into the working folder

                //ToDo Render data
                $scadData = file_get_contents($filename);
                $scadData = $this->replaceScadParams($scadData, $params);

                $copyResult = file_put_contents($this->workingDir . $pathInfo['basename'], $scadData);
                if (!$copyResult) {
                    throw new \Exception('Copying file to working directory failed.');
                }
            }
            // NOTE: Filename will be minus the extension
            $this->file = $pathInfo['filename'];
            $this->fileType = empty($pathInfo['extension']) ? '' : $pathInfo['extension'];
        }
        return $this->file . '.' . $this->fileType;
        //@ return './' . $this->file . '.' . $this->fileType;
    }

    private function replaceScadParams($scadData, $params = []) {
        foreach ($params as $paramName => $paramValue) {
            $scadData = str_replace('##' . strtoupper($paramName) . '##', $paramValue, $scadData);
        }

        return $scadData;
    }
	 
}