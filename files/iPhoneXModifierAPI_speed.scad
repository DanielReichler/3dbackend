text1 = "##TEXT1##";
text2 = "##TEXT2##";
text3 = "##TEXT3##";

//mode = "cut";
//
//font = "Libertas Sans";
//size = 10;
//
//positionX=0;
//positionY=0;
//rotateDegree = 25;

mode = "##MODE##";

font = "##FONTFAMILY##";
size = ##FONTSIZE##;

positionX=##POSX##;
positionY=##POSY##;
rotateDegree =##ROTATEDEGREE##;

// static vars for iPhoneX Case.stl
casewallheight = 0;
casewallthickness = 1.6;

if ( mode == "cut") {   
    difference() {
        importBaseCase();
        cutText(text1, text2, text3, font, size);
    }
}

if ( mode == "impress") {   
    difference() {
        importBaseCase();
        impressText(text1, text2, text3, font, size);
    }
}

module importBaseCase() {
       rotate([0,180,0])
        import_stl("kavianCase.stl", convexity=1, center=true);
}

module cutText(text1, text2, text3) {
    deepthfactor = 1.1;
    securityMargin = 1;

    rotate([0,0, rotateDegree])
    translate([positionX,positionY,-(casewallheight + casewallthickness * deepthfactor) ])
        linear_extrude(height = (casewallthickness * deepthfactor) + securityMargin, convexity = 5)
                multiLine() {
                    if (text1 != "") {
                        text(text1, font = font, size = size, halign = "center");
                    }
                    if (text2 != "") {
                        text(text2, font = font, size = size, halign = "center");
                    }
                    if (text3 != "") {
                        text(text3, font = font, size = size, halign = "center");
                    }
                }
}

module impressText(text1, text2, text3, font, size) {
    deepthfactor = 0.6;
    securityMargin = 1;

    translate([0,0,-(casewallheight + casewallthickness * deepthfactor) ])
        linear_extrude(height = (casewallthickness * deepthfactor) + securityMargin, convexity = 5)
                multiLine() {
                    if (text1 != "") {
                        text(text1, font = font, size = size, halign = "center");
                    }
                    if (text2 != "") {
                        text(text2, font = font, size = size, halign = "center");
                    }
                    if (text3 != "") {
                        text(text3, font = font, size = size, halign = "center");
                    }
                }
}

module multiLine(){
  union(){
    for (i = [0 : $children-1])
      translate([0 , -i * 12, 0 ]) children(i);
  }
}