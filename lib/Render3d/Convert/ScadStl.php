<?php

namespace Render3d\Convert;

use Render3d\Customizer\Render3d;

class ScadStl extends Convert {
	
	public function convert() {
		if ($this->Render3d->fileType() !== 'scad') {
			// Not the right file type to convert
			return;
		}

		//convert using scad
		$openscad = $this->Render3d->executable('openscad');
		$file_stl = $this->Render3d->file() . '.stl';
		$file_scad = $this->Render3d->filename();
		$cmd = "{$openscad} -o \"{$file_stl}\" \"{$file_scad}\"";

		$this->Render3d->cmd($cmd);
		
		if (!file_exists($file_stl)) {
			throw new \Exception("Error creating STL file from SCAD!  Cannot proceed. - " . $cmd);
		}
		$stl_contents = file_get_contents($file_stl);
		if (!strlen($stl_contents)) {
			throw new \Exception("Contents of STL file are empty, convert failed.");
		}
		// Success!  Update the file type
		$this->Render3d->fileType('stl');
	}

    public function apiConvert() {
        if ($this->Render3d->fileType() !== 'scad') {
            // Not the right file type to convert
            return;
        }

        //convert using scad
        $openscad = $this->Render3d->executable('openscad');
        $openscad = $this->Render3d->executable('openscad');
        $file_stl = $this->Render3d->file() . '.stl';
        $file_scad = $this->Render3d->filename();
        $cmd = "{$openscad} -o \"{$file_stl}\" \"{$file_scad}\" --preview=throwntogether";

        $this->Render3d->cmd($cmd);

        if (!file_exists($file_stl)) {
            throw new \Exception("Error creating STL file from SCAD!  Cannot proceed. - " . $cmd);
        }
        $stl_contents = file_get_contents($file_stl);
        if (!strlen($stl_contents)) {
            throw new \Exception("Contents of STL file are empty, convert failed.");
        }
        // Success!  Update the file type
        $this->Render3d->fileType('stl');

        unlink($file_stl);

        return $stl_contents;
    }
}