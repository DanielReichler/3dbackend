<?php

namespace Render3d\Render;

class Dxf extends Render {
	/**
	 * Renders the current file.
	 * 
	 * If render successful, this method should update the Render3d's fileType to match the new file type for the rendered
	 * file.
	 * 
	 * @return string Return the full path to the rendered image, or boolean false if there are any problems
	 * @throws \Exception throws exception if there are problems rendering the image
	 */
	public function render() {
        $result = $this->Render3d->convertTo('dxf');

        //return $this->Render3d->filename();
        return $result;
	}
}